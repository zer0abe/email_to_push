#!/usr/bin/env /usr/bin/python3

import pymysql.cursors
import re
import base64
import json
import logging
import os
import requests
import sys
import datetime
import time
from pprint import pprint
from requests.packages.urllib3.exceptions import InsecureRequestWarning
requests.packages.urllib3.disable_warnings(InsecureRequestWarning)

argv = sys.argv
if not argv.__len__() > 1:
    print("Give me a path to config.json and logs dir")
    exit()

root_path = argv[1]

def filename():
    filename = root_path + 'logs/'
    date = datetime.datetime.now()
    filename += str(date.year) + "_" + str(date.month) + "_" + str(date.day) + ".log"
    return filename


if not os.path.exists(root_path + 'logs/'):
    os.makedirs(root_path + 'logs/')

if not os.path.isfile(filename()):
    os.mknod(filename())


class Config:
    configuration = {}

    def __init__(self):
        with open(root_path + 'config.json', 'r') as cfg:
            self.configuration = json.loads(cfg.read())
            if self.cfg('image', 'save'):
                self.create_img_path()

    def cfg(self, name, key=''):
        if name in self.configuration:
            if key:
                if key in self.configuration[name]:
                    return self.configuration[name][key]
            return self.configuration[name]
        return ''

    def create_img_path(self):
        path = self.cfg('image', 'path')
        if not os.path.isdir(path):
            os.mkdir(path)

logging.basicConfig(format='%(levelname)-8s [%(asctime)s]: %(message)s',
                    filename=filename(),
                    datefmt="%I:%M:%S %p",
                    level=logging.DEBUG)
logging.getLogger('suds').setLevel(logging.INFO)

if argv:
    logging.debug(str(argv))

class Message:
    _camera = ''
    _cam_name = ''
    _db = ''
    _username = ''
    _user_info = {}
    _users = []
    _email = ''
    _cfg = Config()
    filename = ''
    _device_type = ''
    _houses = []
    _snapshot_id = 0

    def __init__(self):
        self._username = self._cfg.cfg('dbmailUsername')
        self._db = pymysql.connect(host=self._cfg.cfg('dbConfig', 'host'),
                                   user=self._cfg.cfg('dbConfig', 'user'),
                                   password=self._cfg.cfg('dbConfig', 'password'),
                                   db=self._cfg.cfg('dbConfig', 'database'),
                                   charset=self._cfg.cfg('dbConfig', 'charset'),
                                   cursorclass=pymysql.cursors.DictCursor)
        self._user_info = self.get_user_info(self._username)
        self._email = self.get_email_parts()

    def get_user_info(self, username):
        try:
            with self._db.cursor() as cursor:
                sql = "SELECT `user_idnr` AS `userId` FROM `dbmail_users` WHERE `userId` = %s"
                cursor.execute(sql, (username,))
                user = cursor.fetchone()
            with self._db.cursor() as cursor:
                sql = "SELECT `mb`.`mailbox_idnr` AS `boxId` FROM `dbmail_mailboxes` `mb` " \
                      "WHERE `mb`.`owner_idnr` = %s"
                cursor.execute(sql, (user['userId'],))
                mailbox = cursor.fetchone()
            if mailbox:
                user.update(mailbox)
                with self._db.cursor() as cursor:
                    # sql = "SELECT `mess`.`physmessage_id`,  `phys`.`messagesize`, `phys`.`internal_date`" \
                    #       " FROM `dbmail_messages` `mess`" \
                    #       " JOIN `dbmail_physmessage` `phys` ON `phys`.`id` = `mess`.`physmessage_id`" \
                    #       " AND `mess`.`mailbox_idnr` = %s" \
                    #       " ORDER BY `mess`.`physmessage_id` DESC LIMIT 1"
                    # cursor.execute(sql, (user['boxId'],))
                    sql = "SELECT `id` as `physmessage_id` FROM `dbmail_physmessage` ORDER BY `id` DESC LIMIT 1"
                    cursor.execute(sql)
                    last_message = cursor.fetchone()
            if last_message:
                user.update(last_message)
            return user
        except pymysql.MySQLError as e:
            logging.error('MySQLError: %s', e.args[0])

    def get_email_parts(self):
        if not self._user_info:
            logging.info('Empty user_info property')
        with self._db.cursor() as cursor:
            sql = "SELECT `parts`.`is_header`, `parts`.`part_key`, `parts`.`part_depth`, " \
                  "`parts`.`part_order`, `parts`.`part_id`, `part`.`data`" \
                  " FROM `dbmail_partlists` `parts` " \
                  "JOIN `dbmail_mimeparts` `part` ON `parts`.`physmessage_id` = %s " \
                  "AND `part`.`id` = `parts`.`part_id` " \
                  "ORDER BY `parts`.`part_key`, `parts`.`part_order` "
            cursor.execute(sql, (self._user_info['physmessage_id'],))
            message_parts = cursor.fetchall()
        if message_parts:
            return message_parts

    def get_email_file(self):
        # sha = hashlib.sha256()
        name = "img_" + str(self._user_info['physmessage_id']) + '_' + str(time.time())
        # sha.update(name.encode())
        # name = sha.hexdigest()
        image_name = self._cfg.cfg('image', 'path') + name
        is_image = False
        image_header_pattern = re.compile('filename="\d*_\d*.jpg')
        camera_id_pattern = re.compile("\((\d*):")
        for part in self._email:
            if part['is_header']:
                image_match = image_header_pattern.findall(part['data'].decode('utf-8'))
                camera_match = camera_id_pattern.findall(part['data'].decode('utf-8'))
                if image_match:
                    is_image = True
                    continue
                if camera_match:
                    self._camera = camera_match[0]
                    continue
            if is_image:
                if self._cfg.cfg('image', 'save'):
                    server = self._cfg.cfg("phpServer", "host")
                    url = "https://" + server + "/chinaApi/screenshot/0/" + str(time.time()) + "/" + self._camera
                    res = requests.put(url, base64.decodebytes(part['data']), verify=False)
                    is_image = False
                    answer = json.loads(res.text)
                    if answer["status"]:
                        self._snapshot_id = answer["snapshot_id"]
                continue

    def cc_request(self, camera=0):
        if camera:
            self._camera = camera
        server = self._cfg.cfg('ccServer', 'host')
        port = self._cfg.cfg('ccServer', 'port')
        headers = {"X-Rubetek-Internal-Key": "test"}
        url = 'https://' + server + ':' + str(port) + '/internal/devices/' + str(self._camera) + '/users/'
        res = requests.get(url, headers=headers, verify=False)
        answer = json.loads(res.text)
        if not answer["device"]["houses"]:
            self._cam_name = self._camera
        else:
            self._cam_name = answer["device"]["houses"][0]["localSettings"]["name"]
        self._users = [user[1:] for user in answer["users"]]
        self._device_type = answer['device']['lastState']['type']
        self._houses = answer['device']['houses']

    def php_request(self):
        if not self._users or not self._houses:
            logging.info('Camera ' + self._camera + ' have no users.')
            return
        server = self._cfg.cfg("phpServer", "host")
        url = 'https://' + server + "/chinaApi/sendPush"
        body = {
            "push_type": 'chc',
            "users": self._users,
            "camera": self._camera,
            "cam_name": self._cam_name,
            "device_name": self._cam_name,
            "device_type": self._device_type,
            "snapshot_id": self._snapshot_id,
            "houses": self._houses
        }
        if self._cfg.cfg('image', 'save'):
            body["image"] = {"name": self.filename, "extension": 'jpg'}
        res = requests.post(url, data=json.dumps(body), verify=False)
        if res.text:
            self.log_info(json.loads(res.text))

    def log_info(self, res):
        if not len(res):
            return
        else:
            msg = u''
            for item in res:
                msg += u"\nCamera ID: %s | Client OS: %s | Client push token: \n%s\nClient locale: %s| Server answer: %s\n\n" \
                     % (item['camera'], item['client']['type_os'], item['client']['push_id'], item['client']['locale'],
                        item['server_answer'])
        logging.info(msg)

message = Message()
message.get_email_file()
message.cc_request(camera=4646655)
message.php_request()
